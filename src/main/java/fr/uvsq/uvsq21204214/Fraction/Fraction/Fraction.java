package fr.uvsq.uvsq21204214.Fraction.Fraction;

final class Fraction {
	private final int denominateur;
	private final int numerateur;
	public static final Fraction ZERO = new Fraction(0,1);
	public static final Fraction UN = new Fraction(1,1);
	public Fraction() {
		this.denominateur=1;
		this.numerateur=0;
	}
	
	public Fraction(int numerateur){
		this.numerateur = numerateur;
		this.denominateur = 1;
	}
	
	public Fraction(int numerateur, int denominateur){
		this.numerateur = numerateur;
		this.denominateur = denominateur;
	}
	
	public double getValeur(){
		return this.numerateur/this.denominateur;
	}
	
	public String toString(){
		return this.numerateur + "/" + this.denominateur + "=" + this.getValeur();
	}
	
	public static boolean egalite(Fraction f1, Fraction f2){
		if(f1.getValeur()==f2.getValeur()){
		return true;
		}
		return false;
	}
	public static boolean estPlusPetit(Fraction f1, Fraction f2){
		if (f1.getValeur()<f2.getValeur()){
			return true;
		}
		return false;
	}
	
	
}
